FROM python:3.7 as builder

ENV CGO_ENABLED=0

WORKDIR /app
COPY . .

RUN pip install -r requirements.txt
RUN apt-get update
RUN apt install ffmpeg -y

ENTRYPOINT [ "python3.7" ]

CMD ["main.py" ]