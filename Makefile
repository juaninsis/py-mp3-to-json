build:
	docker build --no-cache -t py-mp3-to-json -f ./Dockerfile .

run:
	docker run -p 4000:4000 -i py-mp3-to-json

deploy: build run

status := $(if $(shell docker ps -a -q),ok,notok)
docker-clean:
ifeq ($(status), ok)
	@docker stop $(shell docker ps -a -q) && docker rm $(shell docker ps -a -q) && docker volume prune -f
	@echo "the stack of containers was cleaned"
else
	@echo "the stack of containers is empty"
endif