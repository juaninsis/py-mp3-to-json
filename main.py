from flask import Flask, request
from werkzeug.utils import secure_filename
from pydub import AudioSegment
from flask_json import FlaskJSON, json_response
import speech_recognition as sr
import json
import json
import os

GET = "GET"
UPLOAD_FOLDER = './uploads'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['JSON_AS_ASCII'] = False
FlaskJSON(app)
r = sr.Recognizer()

@app.route('/health', methods=[GET])
def health():
    return json.dumps({"status":"ok"})

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    print("start upload file")
    if request.method == 'POST':
        if 'file' not in request.files:
            print("ERROR no file part")
            return json.dumps({"status":"no file part"})
        file = request.files['file']
        if file.filename == '':
            print("ERROR no selectd file")
            return json.dumps({"status":"no selectd file"})
        filename = secure_filename(file.filename)
        full_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(full_path)
        names = file.filename.split('.')
        output_path = full_path[:-3]+"wav"
        if names[2] != "mp3":
            AudioSegment.from_file(full_path).export(output_path, format="wav")
        audio_file = sr.AudioFile(output_path)

        with audio_file as source:
            audio = r.record(source)
            text = r.recognize_google(audio,language="es-ES")
            os.remove(full_path)
            os.remove(output_path)
            return json_response(message=text)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=4000)