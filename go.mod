module gitlab.com/juaninsis/py-mp3-to-json

go 1.19

require (
	github.com/joeshaw/envdecode v0.0.0-20200121155833-099f1fc765bd
	gitlab.com/juaninsis/bb-phone-bill v0.3.0
	gopkg.in/yaml.v2 v2.4.0
)
