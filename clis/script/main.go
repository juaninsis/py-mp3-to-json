package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"time"

	"gitlab.com/juaninsis/py-mp3-to-json/clis/script/cfg"
)

type (
	Response struct {
		User                      *User   `json:"user,omitempty"`
		Calls                     []Call  `json:"calls,omitempty"`
		TotalInternationalSeconds int64   `json:"total_international_seconds"`
		TotalNationalSeconds      int64   `json:"total_national_seconds"`
		TotalFriendsSeconds       int64   `json:"total_friends_seconds"`
		Total                     float64 `json:"total"`
	}
	User struct {
		Address     string   `json:"address"`
		Name        string   `json:"name"`
		PhoneNumber string   `json:"phone_number"`
		Friends     []string `json:"friends,omitempty"`
	}
	Call struct {
		OriginPhoneNumber string    `json:"-"`
		PhoneNumber       string    `json:"destination_phone_number"`
		Duration          int64     `json:"duration"`
		Timestamp         time.Time `json:"timestamp"`
		Amount            float64   `json:"amount"`
	}
)

var (
	input    = cfg.GetInput()
	conf     = cfg.GetConfig()
	isScript = os.Getenv("IS_SCRIPT")
	chCli    = &http.Client{}
	expected = Response{
		TotalInternationalSeconds: 2460,
		TotalNationalSeconds:      11982,
		TotalFriendsSeconds:       2631,
		Total:                     1967.5,
	}
)

func main() {
	var (
		resp string
		err  error
	)

	if resp, err = sendFile(conf.OwnURL+"/upload", input.FileName); err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp)
}

func phoneBillServerCheck(url string) bool {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return false
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := chCli.Do(req)
	if err != nil {
		return false
	}
	defer res.Body.Close()
	return res.StatusCode == http.StatusOK
}

func sendFile(url, fileName string) (str string, err error) {
	values := map[string]io.Reader{
		"file": mustOpen(fileName),
	}
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	for key, r := range values {
		var fw io.Writer
		if x, ok := r.(io.Closer); ok {
			defer x.Close()
		}
		if x, ok := r.(*os.File); ok {
			if fw, err = w.CreateFormFile(key, x.Name()); err != nil {
				return
			}
		} else if fw, err = w.CreateFormField(key); err != nil {
			return
		}
		if _, err = io.Copy(fw, r); err != nil {
			return
		}

	}
	w.Close()

	req, err := http.NewRequest(http.MethodPost, url, &b)
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", w.FormDataContentType())
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	return string(body), nil
}

func getStatus(f bool) string {
	if f {
		return "OK ✅"
	}
	return "ERROR ❌"
}
func mustOpen(f string) *os.File {
	r, err := os.Open(f)
	if err != nil {
		panic(err)
	}
	return r
}
